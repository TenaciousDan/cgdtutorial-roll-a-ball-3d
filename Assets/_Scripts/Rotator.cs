﻿using UnityEngine;

public class Rotator : MonoBehaviour
{
    public Vector3 rotationVector = Vector3.up;
    public float rotationSpeed = 10;
    public Rigidbody body;

    private void Update()
    {
        if (body == null)
            transform.Rotate(rotationVector * Time.deltaTime * rotationSpeed, Space.World);
    }

    private void FixedUpdate()
    {
        if (body != null)
            body.MoveRotation(body.rotation * Quaternion.Euler(transform.InverseTransformVector(rotationVector) * rotationSpeed * Time.fixedDeltaTime));
    }
}
