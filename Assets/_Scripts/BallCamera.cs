﻿using UnityEngine;

public class BallCamera : MonoBehaviour
{
    [SerializeField] private GameObject target;

    [SerializeField, Min(1)]
    private float sensitivity = 100;

    public float smoothing = 5f;

    private float mouseX, mouseY;
    private float distanceToTarget;

    private void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        if (target != null)
        {
            Vector3 offset = this.transform.position - target.transform.position;
            distanceToTarget = offset.magnitude;
            Vector3 xzDirection = Vector3.ProjectOnPlane(offset, Vector3.up);
            mouseX = -Vector3.Angle(xzDirection, -Vector3.forward);
            mouseY = Vector3.Angle(offset, xzDirection);
        }
    }

    private void Update()
    {
        if (target != null)
        {
            mouseX += Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
            mouseY -= Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime; // += for inverted Y

            mouseY = Mathf.Clamp(mouseY, 20, 70);
        }
    }

    private void LateUpdate()
    {
        if (target != null)
        {
            Quaternion desiredRotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(mouseY, mouseX, 0), smoothing * Time.deltaTime);

            transform.position = target.transform.position + desiredRotation * -Vector3.forward * distanceToTarget;
            transform.LookAt(target.transform);
        }
    }
}
