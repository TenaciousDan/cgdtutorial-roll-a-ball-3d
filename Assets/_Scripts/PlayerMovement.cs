﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField, Min(1)]
    private float moveSpeed = 5000;
    public float jumpForce = 5;

    private Rigidbody body;
    private Vector3 movement;

    private bool jumpPressed;
    private bool isGrounded;

    private void Awake()
    {
        body = this.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Camera cam = Camera.main;

        float horizontalAxis = Input.GetAxis("Horizontal") * Time.deltaTime;
        float verticalAxis = Input.GetAxis("Vertical") * Time.deltaTime;

        Vector3 right = Vector3.ProjectOnPlane(cam.transform.right, Vector3.up).normalized;
        Vector3 forward = Vector3.ProjectOnPlane(cam.transform.forward, Vector3.up).normalized;
        movement = right * horizontalAxis + forward * verticalAxis;

        jumpPressed = isGrounded && Input.GetButton("Jump");
    }

    private void FixedUpdate()
    {
        isGrounded = false;
        Vector3 jumpVector = Vector3.up;

        RaycastHit[] hits = Physics.SphereCastAll(transform.position, 1, Vector3.down, 0);
        if (hits != null)
        {
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.tag != "Player" && !hit.collider.isTrigger)
                {
                    Vector3 surfaceNormal = (transform.position - hit.collider.ClosestPoint(transform.position)).normalized;
                    if (Vector3.Angle(surfaceNormal, Vector3.up) <= 60)
                    {
                        jumpVector = surfaceNormal;
                        isGrounded = true;
                        break;
                    }
                }
            }
        }

        if (jumpPressed && isGrounded)
            body.AddForce(jumpVector * jumpForce, ForceMode.Impulse);

        body.AddForce(movement * moveSpeed);
    }
}
