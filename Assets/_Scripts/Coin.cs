﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Level level = transform.root.GetComponent<Level>();
            if (level != null)
                level.Score += 1;

            AudioClip clip = GetComponent<AudioSource>().clip;
            GetComponent<AudioSource>().PlayOneShot(clip);

            GetComponent<MeshRenderer>().enabled = false;
            GetComponent<Collider>().enabled = false;
            Destroy(gameObject, clip.length);
        }
    }
}
