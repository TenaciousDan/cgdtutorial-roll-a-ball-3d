﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsScroller : MonoBehaviour
{
    public float speed = 80;

    private RectTransform rectTransform;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = false;
    }

    private void Start()
    {
        float canvasHeight = ((RectTransform)transform.GetComponentInParent<Canvas>().transform).rect.height;
        rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, -canvasHeight);
    }

    private void Update()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);

        if (rectTransform.anchoredPosition.y > rectTransform.rect.height)
            SceneManager.LoadScene("Welcome");
    }
}
